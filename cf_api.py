import requests
import re
import json
from bs4 import BeautifulSoup
import time
import os

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0'

def get_csrf(client, url, num_iter=3, delay=0.0):
    headers = {
        'User-Agent' : USER_AGENT,
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.get(url, headers=headers)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            gr = re.search(r"""<meta name="X-Csrf-Token" content="(.*?)"/>""", r.text, re.DOTALL)
            if (gr != None):
                csrf_token = gr.group(1)
                result['status'] = 'OK'
                result['result'] = {'csrf_token': csrf_token}
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def login(client, handle, password, csrf_token=None, num_iter=3, delay=0.0):
    if (csrf_token == None):
        data = get_csrf(client, 'https://codeforces.com')
        if (data['status'] == 'FAILED'):
            result = {}
            result['status'] = 'FAILED'
            result['comment'] = {'issue': '"login" failed to get csrf_token', 'description': data['comment']}
            return result
        else:
            csrf_token = data['result']['csrf_token']
    headers = {
        'User-Agent': USER_AGENT,
    }
    data = {
        'csrf_token': csrf_token,
        'action': 'enter',
        'handleOrEmail': handle,
        'password': password,
        'remember': 'on',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.post('https://codeforces.com/enter', headers=headers, data=data)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def get_current_friends(client, num_iter=3, delay=0.0):
    headers = {
        'User-Agent': USER_AGENT,
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.get('https://codeforces.com/friends', headers=headers)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                try:
                    soup = BeautifulSoup(r.text, 'html.parser')
                    soup = soup.find('div', {'class': 'datatable'})
                    soup = soup.find('table').tbody
                    user_list = soup.find_all('tr')
                    users = {}
                    for user in user_list:
                        id = user.find('img', {'class': 'removeFriend friendStar'})['frienduserid']
                        handle = user.find('a').get_text()
                        users[id] = [handle, str(user)]
                    result['status'] = 'OK'
                    result['result'] = {'friends': users}
                    return result
                except:
                    result['status'] = 'FAILED'
                    result['comment'] = {'status_code': r.status_code, 'text': r.text}
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def update_friendship(client, user_id, type, csrf_token=None, num_iter=3, delay=0.0): # type = {'true', 'false'}
    if (csrf_token == None):
        data = get_csrf(client, 'https://codeforces.com')
        if (data['status'] == 'FAILED'):
            result = {}
            result['status'] = 'FAILED'
            result['comment'] = {'issue': '"update_frinedship" failed to get csrf_token', 'description': data['comment']}
            return result
        else:
            csrf_token = data['result']['csrf_token']
    headers = {
        'User-Agent': USER_AGENT,
    }
    params = {
        'friendUserId' : user_id,
        'isAdd' : type,
        'csrf_token' : csrf_token,
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.post('https://codeforces.com/data/friend', headers=headers, params=params)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def check_online(client, handle, num_iter=3, delay=0.0):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.get('https://codeforces.com/profile/%s' % handle, headers=headers)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                if (r.text.find('online now') != -1):
                    result['result'] = 'online'
                else:
                    result['result'] = 'offline'
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def vote_comment(client, commentId, vote, csrf_token=None, num_iter=3, delay=0.0): #vote = {'1', '-1'}
    if (csrf_token == None):
        data = get_csrf(client, 'https://codeforces.com')
        if (data['status'] == 'FAILED'):
            result = {}
            result['status'] = 'FAILED'
            result['comment'] = {'issue': '"vote_comment" failed to get csrf_token', 'description': data['comment']}
            return result
        else:
            csrf_token = data['result']['csrf_token']
    headers = {
        'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0',
    }
    params = {
        'commentId' : commentId,
        'vote' : vote,
        'csrf_token' : csrf_token,
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.post('https://codeforces.com/data/comment/vote', headers=headers, params=params)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                result['message'] = json.loads(r.text)['message']
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def register_for_contest(client, contestId, csrf_token=None, num_iter=3, delay=0.0):
    if (csrf_token == None):
        data = get_csrf(client, 'https://codeforces.com')
        if (data['status'] == 'FAILED'):
            result = {}
            result['status'] = 'FAILED'
            result['comment'] = {'issue': '"vote_comment" failed to get csrf_token', 'description': data['comment']}
            return result
        else:
            csrf_token = data['result']['csrf_token']
    headers = {
        'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0',
    }
    data = {
        'csrf_token' : csrf_token,
        'action' : 'formSubmitted',
        'takePartAs' : 'personal',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.post('https://codeforces.com/contestRegistration/%s' % contestId, headers=headers, files=data)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result