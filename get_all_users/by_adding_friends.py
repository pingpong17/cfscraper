# coding: utf-8

import requests
import re
from bs4 import BeautifulSoup
import json
import time


#import socks
#import socket
#socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
#socket.socket = socks.socksocket

def get_csrf(client, url, num_iter=3, delay=0.0):
    headers = {
        'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.get(url, headers=headers)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            gr = re.search(r"""<meta name="X-Csrf-Token" content="(.*?)"/>""", r.text, re.DOTALL)
            if (gr != None):
                csrf_token = gr.group(1)
                result['status'] = 'OK'
                result['result'] = {'csrf_token': csrf_token}
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def login(client, handle, password, num_iter=3, delay=0.0):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        data = get_csrf(client, 'https://codeforces.com/enter', 1)
        if (data['status'] == 'FAILED'):
            result['status'] = 'FAILED'
            result['comment'] = {'issue': 'filed to get csrf_token', 'description': data['comment']}
        else:
            data = {
                'csrf_token': data['result']['csrf_token'],
                'action': 'enter',
                'handleOrEmail': handle,
                'password': password,
                'remember': 'on',
            }
            try:
                r = client.post('https://codeforces.com/enter', headers=headers, data=data)
            except:
                result['status'] = 'FAILED'
                result['comment'] = 'requests failed to send request'
            else:
                if (r.status_code == 200):
                    result['status'] = 'OK'
                    return result
                else:
                    result['status'] = 'FAILED'
                    result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def get_current_friends(client, num_iter=3, delay=0.0):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.get('https://codeforces.com/friends', headers=headers)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                try:
                    soup = BeautifulSoup(r.text, 'html.parser')
                    soup = soup.find('div', {'class': 'datatable'})
                    soup = soup.find('table').tbody
                    user_list = soup.find_all('tr')
                    users = {}
                    for user in user_list:
                        id = user.find('img', {'class': 'removeFriend friendStar'})['frienduserid']
                        handle = user.find('a').get_text()
                        users[id] = [handle, str(user)]
                    result['status'] = 'OK'
                    result['result'] = {'friends': users}
                    return result
                except:
                    result['status'] = 'FAILED'
                    result['comment'] = {'status_code': r.status_code, 'text': r.text}
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        time.sleep(delay)
    return result


def update_friendship(client, user_id, type, csrf_token, num_iter=3, delay=0.0): # type = {'true', 'false'}
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',
    }
    params = {
        'friendUserId' : user_id,
        'isAdd' : type,
        'csrf_token' : csrf_token,
    }
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        try:
            r = client.post('https://codeforces.com/data/friend', headers=headers, params=params)
        except:
            result['status'] = 'FAILED'
            result['comment'] = 'requests failed to send request'
        else:
            if (r.status_code == 200):
                result['status'] = 'OK'
                return result
            else:
                result['status'] = 'FAILED'
                result['comment'] = {'status_code': r.status_code, 'text': r.text}
        if ('status_code' in result['comment'] and result['comment']['status_code'] == 403):
            print result
            print 'Sleeping...'
            time.sleep(delay)
    return result


def get_id_backup():
    f = open('id_backup.txt', 'r')
    id = f.readline().split()[0]
    f.close()
    f = open('id_backup.txt', 'w')
    f.write(str(int(id) + 1))
    return id


def update_users(client, users, num_iter=3, delay=0.0):
    print 'Updating friends'
    result = {}
    result['status'] = 'FAILED'
    result['comment'] = {'num_iter': num_iter}
    for iter in range(num_iter):
        result = {}
        data = get_current_friends(client, 1)
        if (data['status'] == 'FAILED'):
            result['status'] = 'FAILED'
            result['comment'] = {'issue': 'filed to get current friends', 'description': data['comment']}
        else:
            current_friends = data['result']['friends']
            for user_id in current_friends:
                if (not user_id in users):
                    users[user_id] = current_friends[user_id]
            json.dump(users, open('all_users_by_adding_friends.txt', 'w'))

            id_backup = get_id_backup()
            tmp = open('backup/%s.txt' % str(int(id_backup) - 5), 'w'); tmp.close()
            json.dump(users, open('backup/%s.txt' % id_backup, 'w'))

            result['status'] = 'OK'
            print 'Succeeded to update friends'
            return result
        time.sleep(delay)
    print 'Failed to update friends'
    return result



def delete_friends(client, users):
    print 'Deleting friends'

    data = get_current_friends(client, 60, 30)
    if (data['status'] != 'OK'):
        print 'Function "delete_friends" failed to get current friends'
        raise Exception
    current_friends = data['result']['friends']

    data = get_csrf(client, 'https://codeforces.com', 60, 30)
    if (data['status'] != 'OK'):
        print 'Function "delete_friends" failed to get csrf_token'
        raise Exception
    csrf_token = data['result']['csrf_token']

    for user_id in current_friends:
        data = update_friendship(client, user_id, 'false', csrf_token, 30, 30)
        if (data['status'] != 'OK'):
            data = get_csrf(client, 'https://codeforces.com', 60, 30)
            if (data['status'] != 'OK'):
                print 'Function "delete_friends" failed to get csrf_token'
                raise Exception
            csrf_token = data['result']['csrf_token']
            data = update_friendship(client, user_id, 'false', csrf_token, 30, 30)
        if (data['status'] != 'OK'):
            print 'Failed to delete user_id: %s from friends' % user_id
            raise Exception
    return {'status': 'OK'}


max_id = 0


def add_friends(client, users, limit):
    global max_id
    id = 0
    for user_id in users:
        id = max(id, int(user_id))
    id = max(id, max_id)
    id += 1
    if (id > limit):
        return {'status': 'OK', 'comment': 'no users to add'}
    max_id = id + 499
    print 'Adding friends ids: %s-%s' % (id, min(id + 499, limit))

    data = get_csrf(client, 'https://codeforces.com', 60, 30)
    if (data['status'] != 'OK'):
        print 'Function "add_friends" failed to get csrf_token'
        raise Exception
    csrf_token = data['result']['csrf_token']

    for t in range(min(500, limit - id + 1)):
        data = update_friendship(client, id + t, 'true', csrf_token, 30, 30)
        if (data['status'] != 'OK'):
            data = get_csrf(client, 'https://codeforces.com', 60, 30)
            if (data['status'] != 'OK'):
                print 'Function "add_friends" failed to get csrf_token'
                raise Exception
            csrf_token = data['result']['csrf_token']
            data = update_friendship(client, id + t, 'true', csrf_token, 30, 30)
        if (data['status'] != 'OK'):
            print 'Failed to add user_id: %s to friends' % (id + t)
            raise Exception
    return {'status': 'OK'}


def main():
    users = json.loads(open('all_users_by_adding_friends.txt', 'r').readline())

    print 'Logging in'
    client = requests.session()
    data = login(client, 'up-and-down', 'tupic239', 60, 30)
    if (data['status'] == 'OK'):
        print 'Successfully logged in'
    else:
        print 'Failed to log in'
        print data['comment']
        return

    data = update_users(client, users, 60, 30)
    if (data['status'] == 'OK'):
        print 'Successfully updated users'
    else:
        print 'Failed to update users'
        print data['comment']
        return

    limit = 700000
    while (True):
        delete_friends(client, users)
        data = add_friends(client, users, limit)
        if ('comment' in data and data['comment'] == 'no users to add'):
            break
        update_users(client, users, 60, 30)
        if (data['status'] == 'OK'):
            print 'Successfully updated users'
        else:
            print 'Failed to update users'
            print data['comment']
            return

main()