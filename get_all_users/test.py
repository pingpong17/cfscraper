# coding: utf-8

import requests
import re
import json
from bs4 import BeautifulSoup
import time

all_users = json.loads(open('all_users.txt', 'r').readline())

data = json.loads(open('all_users_by_adding_friends1.txt', 'r').readline())

bad_ids = []

a = 0
for id in data:
    user = data[id]
    if (type(user) == type([])):
        continue
        gr = re.search(r'''title="(.*?)"''', user[1], re.DOTALL)
        v = gr.group(1)[:-len(data[id][0]) - 1]
        if (v[-1] == ','):
            v = v[:-1]
        new_user = {
            'id': int(id),
            'handle': user[0],
            'title': v,
        }
        all_users[id] = new_user
    else:
        if (not id in all_users):
            bad_ids.append(id)

print len(bad_ids)
print bad_ids
#json.dump(bad_ids, open('bad_ids.txt', 'w'))

#json.dump(all_users, open('all_users.txt', 'w'))
exit(0)